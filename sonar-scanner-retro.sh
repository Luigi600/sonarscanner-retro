#/bin/bash

args=$@

commits=$(git log --pretty=format:"%H %cd" --date=iso-strict --reverse)
first_iteration=1

last_commit=000000
last_commit_date=0

sonarqube_check() {
    c_date=$(date -d "$2" +"%Y-%m-%d")
    echo "CHECK $1 ($c_date) ($2)"

    git checkout $1
    # source: https://stackoverflow.com/a/61951854
    sonar-scanner ${args} -Dsonar.projectDate="${c_date}"
}

# source: https://superuser.com/a/284226
while IFS= read -r line; do
    commit_stats=($line)
    commit=${commit_stats[0]}
    date=$(date -d "${commit_stats[1]}")

    if [ $first_iteration -eq 1 ] || [ $(date -d "$last_commit_date" +"%Y-%m-%d") == $(date -d "$date" +"%Y-%m-%d") ]; then
        first_iteration=0
        last_commit="${commit}"
        last_commit_date="${date}"
    # elif [ $(date -d "$last_commit_date" +"%Y-%m-%d") == $(date -d "$date" +"%Y-%m-%d") ]; then
        # echo "SAME DATE"
    elif [ $(date -d "$last_commit_date" +"%Y-%m-%d") != $(date -d "$date" +"%Y-%m-%d") ]; then
        # echo "CHECK $last_commit"
        sonarqube_check "${last_commit}" "${last_commit_date}"

        last_commit="${commit}"
        last_commit_date="${date}"
    fi
done <<< "$commits"

if [ $first_iteration -eq 0 ]; then
    sonarqube_check "${last_commit}" "${last_commit_date}"
fi
