# container
ARG SCANNER_VERSION="4.8"

FROM sonarsource/sonar-scanner-cli:${SCANNER_VERSION}

# source: https://unix.stackexchange.com/a/206620
RUN apk add --update coreutils

COPY --chmod=0750 sonar-scanner-retro.sh /usr/local/bin/sonar-scanner-retro
